Feature: Create Lead

Background:
Given Open the Browser
And Max the Browser
And Set the TimeOut
And Launch the URL

Scenario Outline: Positive Create Lead
And Enter the Username as <userName>
And Enter the Password as <password>
And Click on the Login Button
And Click on CRMSFA Button
And Click Leads Button
And Click on Create Leads Button
And Enter Company Name as <companyName>
And Enter First Name as <firstName>
And Enter Last Name as <lastName>
When Click on CreateLead Button
Then Verify the Created Lead

Examples:
|userName|password|companyName|firstName|lastName|
|DemoSalesManager|crmsfa|QWE|Steven|Spielberg|
|DemoSalesManager|crmsfa|QWE|David|Fincher|
