package steps;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Flipkart {
	ChromeDriver driver;
	String eleNewfirstProduct;
	@Given("Open the Browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();	        
	}

	@Given("Max the Browser")
	public void maxTheBrowser() {
		driver.manage().window().maximize();	        
	}

	@Given("Set the TimeOut")
	public void setTheTimeOut() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	        
	}
  
	@Given("Launch Flipkart URL")
	public void launchFlipkartURL() {
		driver.get("http://www.flipkart.com");	
	}

	@Given("Close the Popup")
	public void closeThePopup() {
		driver.findElementByXPath("//button[@class=\"_2AkmmA _29YdH8\"]").click();
		}

	@Given("Mouse Over On Electronics")
	public void mouseOverOnElectronics() {
		WebElement eleElectronics = driver.findElementByXPath("(//span[@class=\"_1QZ6fC _3Lgyp8\"])[1]");
		Actions builder = new Actions(driver);
		builder.moveToElement(eleElectronics).perform();
	}

	@Given("Click Mi")
	public void clickMi() {
		driver.findElementByLinkText("Mi").click();

	}

	@Given("Click Newest First")
	public void clickNewestFirst() throws InterruptedException {
		driver.findElementByXPath("//div[text()=\"Newest First\"]").click();
		Thread.sleep(300);	    

	}

	@Given("Verify Product Page Title")
	public void verifyProductPageTitle() {
		String miTitlePage = driver.findElementByXPath("(//h1[@class=\"_2yAnYN\"])").getText();
		if (miTitlePage.equals("Mi Mobiles")) {
			System.out.println("The Tile is Verified");
			}
		else {
			System.out.println("The Title is not correct");
		}	    

	}

	@Given("Print All Products Prices")
	public void printAllProductsPrices() {
		List<WebElement> productNames = driver.findElementsByXPath("//div[@class=\"_3wU53n\"]");
		List<WebElement> mobilePrice = driver.findElementsByXPath("//div[@class=\"_1vC4OE _2rQ-NK\"]");
		
		for (int i = 0; i < productNames.size(); i++) {
			System.out.println("Mobile name: "+productNames.get(i).getText() +"  Mobile price:"+mobilePrice.get(i).getText());
		}	    

	}

	@Given("Click First Product")
	public void clickFirstProduct() throws InterruptedException {
		WebElement eleNewestProducts = driver.findElementByXPath("(//div[@class=\"_3wU53n\"])[1]");
		eleNewestProducts.click();
		eleNewfirstProduct = eleNewestProducts.getText();
		Thread.sleep(200);
	}

	@Given("Switch to New Window")
	public void switchToNewWindow() {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<String>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(1));
	}

	@Given("Verify Product Title")
	public void verifyProductTitle() {
		String eleProductTitle = driver.findElementByXPath("//span[@class=\"_35KyD6\"]").getText();
		if(eleProductTitle.contains(eleNewfirstProduct)) {
			System.out.println("The Product Title matches");
		}
		else {
			System.out.println("The product title does not match");
		}
	}

	@When("Print Reviews Count")
	public void printReviewsCount() {
	
		List<WebElement> eleRatingReviews = driver.findElementsByXPath("//div[@class=\"col-12-12\"]");
		for (int i = 0; i < eleRatingReviews.size(); i++) {
			System.out.println(eleRatingReviews.get(i).getText());
		}
	}

	@Then("Close the Browser")
	public void closeTheBrowser() {
		driver.close();
	}
}
