//package steps;
//
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.chrome.ChromeDriver;
//
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class CreateLead {
//	
//	public ChromeDriver driver;
//	
//	@Given("Open the Browser")
//	public void openTheBrowser() {
//		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
//		driver = new ChromeDriver();	        
//	}
//
//	@Given("Max the Browser")
//	public void maxTheBrowser() {
//		driver.manage().window().maximize();	        
//	}
//
//	@Given("Set the TimeOut")
//	public void setTheTimeOut() {
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	        
//	}
//
//	@Given("Launch the URL")
//	public void launchTheURL() {
//		driver.get("http://leaftaps.com/opentaps");	       
//	}
//
//	@Given("Enter the Username as (.*)")
//	public void enterTheUsernameAsDemoSalesManager(String data) {
//		driver.findElementById("username").sendKeys(data);	    
//	}
//
//	@Given("Enter the Password as (.*)")
//	public void enterThePasswordAsCrmsfa(String data) {
//		driver.findElementById("password").sendKeys(data);    	    
//	}
//
//	@Given("Click on the Login Button")
//	public void clickOnTheLoginButton() {
//		driver.findElementByClassName("decorativeSubmit").click();  
//	}
//
//	@Given("Click on CRMSFA Button")
//	public void clickOnCRMSFAButton() {
//		driver.findElementByLinkText("CRM/SFA").click();	        
//	}
//
//	@Given("Click Leads Button")
//	public void clickLeadsButton() {
//	    driver.findElementByLinkText("Leads").click();
//	}
//
//	@Given("Click on Create Leads Button")
//	public void clickOnCreateLeadsButton() {
//	    driver.findElementByLinkText("Create Lead").click();  
//	}
//
//	@Given("Enter Company Name as (.*)")
//	public void enterCompanyNameAsQWE(String data) {
//		driver.findElementById("createLeadForm_companyName").sendKeys(data);    
//	}
//
//	@Given("Enter First Name as (.*)")
//	public void enterFirstNameAsSteven(String data) {
//		driver.findElementById("createLeadForm_firstName").sendKeys(data);
//	}
//
//	@Given("Enter Last Name as (.*)")
//	public void enterLastNameAsSpielberg(String data) {    
//		driver.findElementById("createLeadForm_lastName").sendKeys(data);
//	}
//
//	@When("Click on CreateLead Button")
//	public void clickOnCreateLeadButton() {
//	    driver.findElementByClassName("smallSubmit").click();
//	}
//
//	@Then("Verify the Created Lead")
//	public void verifyTheCreatedLead() {
//		String displayedfirstName = driver.findElementById("viewLead_firstName_sp").getText();
//		System.out.println(displayedfirstName);	    
//	}
//
//}
