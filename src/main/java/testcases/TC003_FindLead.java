package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_FindLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_FindLead";
		testDescription = "FindLead";
		authors = "Sasi";
		category = "Smoke";
		dataSheetName = "TC001";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clicktoFindLeads()
		.findUsingFirstName("Sasi")
		.clickFindLead();
}
}
