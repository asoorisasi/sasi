package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "Sasi";
		category = "Smoke";
		dataSheetName = "TC001";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName("XYZ")
		.enterFirstName("Sasikumar")
		.enterLastName("A")
		.CreateLead();
}
}
