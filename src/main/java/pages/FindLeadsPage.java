package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "(//input[@name=\"firstName\"])[3]") WebElement eleUsingFirstName;
	@FindBy(how = How.XPATH, using = "//button[text()=\"Find Leads\"]") WebElement eleFindLead;
	@FindBy(how = How.LINK_TEXT,using="10255") WebElement eleFirstLead;
	@FindBy(how = How.LINK_TEXT,using="10268") WebElement eleSecondLead;
	
	

	public FindLeadsPage findUsingFirstName(String data) {
		type(eleUsingFirstName,data);
		return this;
	}
	public FindLeadsPage clickFindLead() {
		click(eleFindLead);
	return this;
	}
	public MergeLeadsPage clickFromLead() {
		click(eleFirstLead);
		return new MergeLeadsPage();
	}
	public MergeLeadsPage clickToLead() {
		click(eleSecondLead);
		return new MergeLeadsPage();
	}
	
	
}
