package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "(//img[@src=\"/images/fieldlookup.gif\"])[1]") WebElement eleFromLead;
	@FindBy(how = How.XPATH, using = "(//img[@src=\"/images/fieldlookup.gif\"])[2]") WebElement eleToLead;
	@FindBy(how = How.LINK_TEXT,using="Merge") WebElement eleMerge;
	
	public FindLeadsPage fromLead() {
	 click(eleFromLead);
	return new FindLeadsPage();

}
	public FindLeadsPage toLead() {
		 click(eleToLead);
		return new FindLeadsPage();

	}
	
	public ViewLeadsPage merge() {
		click(eleToLead);
		 acceptAlert();
		return new ViewLeadsPage();		
	}
}
