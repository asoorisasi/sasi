package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement eleCreateLead;
	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement eleFindLeads;
	@FindBy(how = How.LINK_TEXT, using = "Find Leads") 
	WebElement eleToFindLeads;
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads") 
	WebElement eleMergeLeads;
	
	public CreateLeadPage clickCreateLead() {		
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	public FindLeadsPage clickFindLeads() {		
		click(eleFindLeads);
		return new FindLeadsPage();
	}
	public FindLeadsPage clicktoFindLeads() {
		click(eleToFindLeads);
	return new FindLeadsPage();
	}
	public MergeLeadsPage clickMergeLeads() {
		click(eleMergeLeads);
	return new MergeLeadsPage();





	}


}
