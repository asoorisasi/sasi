package flipkart;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FlipkartMi {
	
	public static void main(String[] args) throws InterruptedException  {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.flipkart.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement eleAlert = driver.findElementByXPath("//button[@class=\"_2AkmmA _29YdH8\"]");
		eleAlert.click();
		WebElement eleElectronics = driver.findElementByXPath("(//span[@class=\"_1QZ6fC _3Lgyp8\"])[1]");
		Actions builder = new Actions(driver);
		builder.moveToElement(eleElectronics).perform();
		WebElement eleMi = driver.findElementByLinkText("Mi");
		eleMi.click();
		String miTitlePage = driver.findElementByXPath("(//h1[@class=\"_2yAnYN\"])").getText();
		if (miTitlePage.equals("Mi Mobiles")) {
			System.out.println("The Tile is Verified");
			}
		else {
			System.out.println("The Title is not correct");
		}
		driver.findElementByXPath("//div[text()=\"Newest First\"]").click();
		Thread.sleep(300);
		List<WebElement> productNames = driver.findElementsByXPath("//div[@class=\"_3wU53n\"]");
		List<WebElement> mobilePrice = driver.findElementsByXPath("//div[@class=\"_1vC4OE _2rQ-NK\"]");
		
		for (int i = 0; i < productNames.size(); i++) {
			System.out.println("Mobile name: "+productNames.get(i).getText() +"  Mobile price:"+mobilePrice.get(i).getText());
		}
		
		WebElement eleNewestProducts = driver.findElementByXPath("(//div[@class=\"_3wU53n\"])[1]");
		eleNewestProducts.click();
		String eleNewfirstProduct = eleNewestProducts.getText();
		Thread.sleep(200);
		
		
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<String>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(1));
		String eleProductTitle = driver.findElementByXPath("//span[@class=\"_35KyD6\"]").getText();
		if(eleProductTitle.contains(eleNewfirstProduct)) {
			System.out.println("The Product Title matches");
		}
		else {
			System.out.println("The product title does not match");
		}
		List<WebElement> eleRatingReviews = driver.findElementsByXPath("//div[@class=\"col-12-12\"]");
		for (int i = 0; i < eleRatingReviews.size(); i++) {
			System.out.println(eleRatingReviews.get(i).getText());
		}
		driver.close();
	}
}
	

